%global	fontname jomolhari
%global fontconf 65-0-jomolhari.conf

Name:           jomolhari-fonts
Version:        0.003
Release:        28
Summary:        A Bhutanese font style for Tibetan and Dzongkha

License:        OFL
URL:            https://sites.google.com/site/chrisfynn2/home/fonts/jomolhari
Source0:        https://collab.its.virginia.edu/access/content/group/26a34146-33a6-48ce-001e-f16ce7908a6a/Tibetan%20fonts/Tibetan%20Unicode%20Fonts/Jomolhari-alpha003.zip
Source1:        jomolhari-fonts-fontconfig.conf

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
Jomolhari is an TrueType OpenType Bhutanese style font for Dzongkha and Tibetan
text. It supports the Unicode and the Chinese encoding for Tibetan.

%package_help

%prep
%autosetup -c -n %{name}-%{version}

%build

%install
pushd %{buildroot}
install -m 0755 -d .%{_fontdir}
install -m 0644 -p %{_builddir}/%{buildsubdir}/*.ttf .%{_fontdir}
install -m 0755 -d .%{_fontconfig_templatedir} .%{_fontconfig_confdir}
install -m 0644 -p %{SOURCE1} .%{_fontconfig_templatedir}/%{fontconf}
popd

ln -s %{_fontconfig_templatedir}/%{fontconf} \
	%{buildroot}%{_fontconfig_confdir}/%{fontconf}

%define txtfiles {FONTLOG.txt OFL-FAQ.txt OFL.txt}
for i in %txtfiles
do
        tr -d '\r' < $i > ${i}.tmp
        mv -f ${i}.tmp $i
done

%files
%defattr(-,root,root)
%doc OFL.txt
%{_sysconfdir}/fonts/conf.d/65-0-jomolhari.conf
%{_datadir}/fonts/jomolhari/*.ttf
%{_datadir}/fontconfig/conf.avail/65-0-jomolhari.conf

%files help
%defattr(-,root,root)
%doc FONTLOG.txt OFL-FAQ.txt

%changelog
* Thu Nov 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.003-28
- Package init
